<h3> Pengumpulan Tugas Sanbercode - Laravel<h3> Pengumpulan Tugas Sanbercode - Laravel

Nama : Aqshal Deandra Vandaru
<br>

<ins> <b> Hari 07 </b> </ins>
1. [String](https://gitlab.com/aqshal.deandra/sanbercode-laravel-batch29/-/blob/master/Hari07-string.php)
2. [Array](https://gitlab.com/aqshal.deandra/sanbercode-laravel-batch29/-/blob/master/Hari07-array.php)

<ins> <b> Hari 08 </b> </ins>
1. [Looping](https://gitlab.com/aqshal.deandra/sanbercode-laravel-batch29/-/blob/master/Hari08-looping.php)
2. [Function & Conditional](https://gitlab.com/aqshal.deandra/sanbercode-laravel-batch29/-/blob/master/Hari08-function-conditional.php)
